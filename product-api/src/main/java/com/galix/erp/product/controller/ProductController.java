package com.galix.erp.product.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.galix.erp.commons.entity.spec.MapMap;
import com.galix.erp.commons.entity.spec.ParamType;
import com.galix.erp.commons.entity.spec.Parameter;
import com.galix.erp.commons.product.entity.Product;
import com.galix.erp.commons.ws.client.service.MapperService;
import com.galix.erp.commons.ws.client.service.RequestService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Product")
@RestController
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private MapperService mapperService;

	@Autowired
	private RequestService requestService;

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(
			value = "Returns all products", 
			notes = "Returns a complete list of the products.", 
			response = MapMap.class)
	public MapMap findAll() throws JAXBException, IOException, XMLStreamException {
		List<Parameter> params = new ArrayList<>();
		params.add(new Parameter("searchByProductIdValue", "", ParamType.STRING));
		return RestPreconditions.checkFound(
				requestService.invoke("findProducts", "findProducts", params));
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(
			value = "Returns the products by id", 
			notes = "Returns a complete list of the products.", 
			response = MapMap.class)
	public MapMap findById(@PathVariable("id") String id) throws JAXBException, IOException, XMLStreamException {
		List<Parameter> params = new ArrayList<>();
		params.add(new Parameter("idToFind", id, ParamType.STRING));
		return RestPreconditions.checkFound(
				requestService.invoke("findProductById", "findProductById", params));
	}
	
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(
			value = "Creates a new product.", 
			notes = "Creates a new product.", 
			response = MapMap.class)
	public MapMap createProduct(@Valid @RequestBody Product product) throws JAXBException, IOException, XMLStreamException {
		return RestPreconditions.checkFound(
				requestService.invoke("createProduct", "createProduct", mapperService.mapObjectToParams(product)));
	}
}
