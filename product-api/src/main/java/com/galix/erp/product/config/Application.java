package com.galix.erp.product.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
		scanBasePackages = {
				"com.galix.erp.product",
				"com.galix.erp.commons"
				})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
